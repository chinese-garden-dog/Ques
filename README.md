#Ques
1、利用mui,jqery,php,mysql等技术实现填空、选择、折叠等常见问卷题型的后台生成、前台展示等功能
2、简洁、美观、易用是我的目标
![首页](https://git.oschina.net/uploads/images/2017/1012/105413_329136b5_459190.png "首页.png")![填空](https://git.oschina.net/uploads/images/2017/1012/105435_0f5f4a04_459190.png "填空.png")![数字输入](https://git.oschina.net/uploads/images/2017/1012/105501_d97b9539_459190.png "数字输入.png")![单选](https://git.oschina.net/uploads/images/2017/1012/105529_fb7221ac_459190.png "单选.png")![折叠题](https://git.oschina.net/uploads/images/2017/1012/105543_7d8682e1_459190.png "折叠题包含填已有的题型.png")